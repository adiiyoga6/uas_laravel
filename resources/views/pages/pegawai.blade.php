@include('includes.headpages')

<body class="theme-light-blue">
    {{-- Page Loader --}}
    @include('includes.loading')
    {{-- #END# Page Loader --}}
    {{-- Overlay For Sidebars --}}
    <div class="overlay"></div>
    {{-- #END# Overlay For Sidebars --}}
    {{-- Top Bar --}}
    @include('includes.nav')
    {{-- #Top Bar --}}
    <section>
        {{-- Left Sidebar --}}
        @include('includes.sidebar')
        {{-- #END# Left Sidebar --}}
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>PEGAWAI</h2>
            </div>
            {{-- Vertical Layout --}}
            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-10 col-xs-12">
                   
                    <div class="card">
                        <div class="header">
                            <h2>
                                TAMBAH DATA PEGAWAI
                            </h2>
                        </div>
                        <div class="body">
                            <form action="" method="post">
                                <label for="NIP">NIP</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="NIP" class="form-control" name="no_pegawai" required>
                                    </div>
                                </div>
                                <label for="namapegawai">Nama Lengkap</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="namapegawai" class="form-control" name="nama_pegawai" required>
                                    </div>
                                </div>
                                <label for="asal">Asal</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="asal" class="form-control" name="asal">
                                    </div>
                                </div>
                                
                                <br>
                                <button type="submit" class="btn btn-primary m-t-15 waves-effect" name="simpan">SIMPAN</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Basic Table --}}
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                List Pegawai
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>NIP</th>
                                        <th>Nama</th>
                                        <th>Asal</th>
                                        
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                          <td></td>
                                          <td></td>
                                          <td></td>
                                          <td>
                                            <a href=""><button type="button" class="btn bg-green btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">edit</i>
                                            </button></a>
                                          </td>
                                        </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {{-- #END# Basic Table --}}

            </div>
        </div>
    </section>
@include('includes.footjspages')
