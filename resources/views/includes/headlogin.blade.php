<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Login</title>
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    
    <link href="{{ asset('layout/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    
    <link href="{{ asset('layout/plugins/node-waves/waves.css') }}" rel="stylesheet" />
    
    <link href="{{ asset('layout/plugins/animate-css/animate.css') }}" rel="stylesheet" />
    
    <link href="{{ asset('layout/css/style.css') }}" rel="stylesheet">
</head>