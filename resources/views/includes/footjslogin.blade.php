<script src="{{ asset('layout/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('layout/plugins/bootstrap/js/bootstrap.js') }}"></script>

    <script src="{{ asset('layout/plugins/node-waves/waves.js') }}"></script>

    <script src="{{ asset('layout/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <script src="{{ asset('layout/js/admin.js') }}"></script>
    <script src="{{ asset('layout/js/pages/examples/sign-in.js') }}"></script>
</body>

</html>