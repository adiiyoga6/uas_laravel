<aside id="leftsidebar" class="sidebar">
    {{-- User Info --}}
    <div class="user-info">
        <div class="image">
                <img src="{{ asset('layout/images/avatar1.jpg') }}" width="55" height="55" alt="User" />
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hi, Adi</div>
        </div>
    </div>
    {{-- #User Info --}}
    {{-- Menu --}}
    <div class="menu">
        <ul class="list">
            <li >
                <a href="#">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            <li class="active">
                <a href="">
                    <i class="material-icons">face</i>
                    <span>Pegawai</span>
                </a>
            </li>
        </ul>
    </div>
    {{-- #Menu --}}
    {{-- Footer --}}
    @include('includes.footer')
    {{-- #Footer --}}
</aside>